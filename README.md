Sandbox to play with Vertex shaders
===================================

This depends on `py3d` which you can git clone at:
git@github.com:ax-va/PyOpenGL-Pygame-Stemkovski-Pascale-2022.git

This module is not directly installable, you have to add it to your PYTHONPATH environment variable.

The rest of the dependencies are in requirements.txt.

The full setup should look like this:
``` bash
git clone git@github.com:ax-va/PyOpenGL-Pygame-Stemkovski-Pascale-2022.git ../py3d/
export PYTHONPATH=../py3d
pip install -r requirements.txt
```

`py3d` is based on  Developing Graphics Frameworks with Python and OpenGL. The electronic version of the book is available for free:
https://www.routledge.com/Developing-Graphics-Frameworks-with-Python-and-OpenGL/Stemkoski-Pascale/p/book/9780367721800

