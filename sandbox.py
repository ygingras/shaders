
# this code is heavily inspired by example_4_3.py from py3d

from time import time
from math import pi
import numpy as np

from py3d.core.base import Base
from py3d.core_ext.camera import Camera
from py3d.core_ext.mesh import Mesh
from py3d.core_ext.renderer import Renderer
from py3d.core_ext.scene import Scene
from py3d.geometry.geometry import Geometry
from py3d.material.point import PointMaterial
from py3d.material.line import LineMaterial


COOL_V_SHADER = """
        uniform mat4 projectionMatrix;
        uniform mat4 viewMatrix;
        uniform mat4 modelMatrix;
        in vec3 v_pos;
        uniform float time;
        uniform float offset;

        void main()
        {
            
            vec3 new_pos = vec3(v_pos.x*sin(time+offset), v_pos.y*cos(time+offset), v_pos.z);
            gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(new_pos, 1.0);
        }
"""

PLAIN_V_SHADER = """
        uniform mat4 projectionMatrix;
        uniform mat4 viewMatrix;
        uniform mat4 modelMatrix;
        in vec3 v_pos;

        void main()
        {
            gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(v_pos, 1.0);
        }
"""
        
FRAGMENT_SHADER = """
        uniform vec3 baseColor;
        uniform bool useVertexColors;
        out vec4 fragColor;
        uniform float time;
        void main()
        {
            float r = clamp(abs(baseColor.r - time), 0.0, 1.0);
            // float b = ... ;
            vec3 col = vec3(r, baseColor.g, baseColor.b);
            fragColor = vec4(col, 1.0);
        }
"""

class ShaderSandbox(Base):
    """ Render the sine function """
    def initialize(self):
        print("Initializing program...")
        self.renderer = Renderer()
        self.scene = Scene()
        self.camera = Camera(aspect_ratio=800/600)
        self.camera.set_position([0, 0, 5])
        self.time = time()

        geometry = Geometry()
        position_data = []
        x_values = np.arange(-pi, pi, 0.2)
        y_values = np.sin(x_values)
        for x, y in zip(x_values, y_values):
            position_data.append([x, y, 0])
        geometry.add_attribute("vec3", "v_pos", position_data)
        geometry.count_vertices()
        
        material = PointMaterial(COOL_V_SHADER, FRAGMENT_SHADER,
                                       {"baseColor": [1, 1, 0], "pointSize": 10})
        material.add_uniform("float", "time", self.time)
        material.add_uniform("float", "offset", 0.0)
        material.locate_uniforms()
        self.mesh1 = Mesh(geometry, material)
        self.scene.add(self.mesh1)

        material = PointMaterial(COOL_V_SHADER, FRAGMENT_SHADER,
                                       {"baseColor": [0, 0, 1], "pointSize": 10})
        material.add_uniform("float", "time", self.time)
        material.add_uniform("float", "offset", pi/2.0)
        material.locate_uniforms()
        self.mesh2 = Mesh(geometry, material)
        self.scene.add(self.mesh2)

    def update(self):
        
        self.time = time() % pi
        self.mesh1.material.uniform_dict["time"].data = self.time
        self.mesh2.material.uniform_dict["time"].data = self.time
        self.renderer.render(self.scene, self.camera)


def main():
    ShaderSandbox(screen_size=[1024, 768]).run()


if __name__ == "__main__":
    main()


